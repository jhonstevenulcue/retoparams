import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostCreateComponent } from './post-create/post-create.component';
import { PostListComponent } from './post-list/post-list.component';
import { PostUpdateComponent } from './post-update/post-update.component';
import { PostViewComponent } from './post-view/post-view.component';
import { PostRoutingModule } from './post-routing.module';
import { ReactiveFormsModule } from '@angular/forms'



@NgModule({
  declarations: 
  [PostCreateComponent, 
  PostListComponent, 
  PostUpdateComponent, 
  PostViewComponent],
  imports: [
    CommonModule,
    PostRoutingModule,
    ReactiveFormsModule
  ]
})
export class PostModule { }
