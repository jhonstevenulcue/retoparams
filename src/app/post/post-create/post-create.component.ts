import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Post } from '../interfaz/post';
import { PostService } from '../post.service'

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.styl']
})
export class PostCreateComponent implements OnInit {

  postCreateFormGroup: FormGroup;

  post : Post;

  statusSearchUserId: Boolean = false;

  formsearchPost = this.formBuilder.group({
    userId: ['']
  });
    
  constructor(private formBuilder: FormBuilder, private PostService: PostService) { }

  ngOnInit() {
  }

  searchUserId(){
    console.warn('DATA', this.formsearchPost.value);
    this.PostService.getPostByUserId(this.formsearchPost.value.userId)
    .subscribe(res =>{
      console.warn('reponse comments by postId ', res);
      this.post = res;
      this.statusSearchUserId = true;
    }, error => {
      console.warn('No encontrado ', error);
      this.post = null;
      this.statusSearchUserId = false;
    });
  }

}


