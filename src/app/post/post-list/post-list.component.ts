import { Component, OnInit } from '@angular/core';
import { Post} from '../interfaz/post'
import { PostService } from '../post.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.styl']
})
export class PostListComponent implements OnInit {

  public postList: Post[];

  constructor(private postService: PostService) { }

  ngOnInit() {
    this.postService.query()
    .subscribe(res => {
      this.postList = res;
      console.log('response data', res);
    },
    error => console.error('error', error)
    );
  }

}
