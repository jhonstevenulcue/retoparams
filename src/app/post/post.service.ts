import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from './interfaz/post';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor( private http: HttpClient ) { }

  public query(): Observable<Post[]> {
    return this.http.get<Post[]>(`${environment.ENN_POINT}/posts`)
    .pipe(map(res => {
      return res;
    }));
  }
  public getPostByUserId(userId: string): Observable<Post>{
    let params = new HttpParams();
    params = params.append('userId',userId);
    console.warn('PARAMS ',params);
    return this.http.get<Post>(`${environment.ENN_POINT}/posts`,{params: params})
      .pipe(map(res => {
        return res;
      }));
    }

}
