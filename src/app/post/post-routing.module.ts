import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostListComponent } from './post-list/post-list.component';
import { PostCreateComponent } from './post-create/post-create.component';
import { PostUpdateComponent } from './post-update/post-update.component';
import { PostViewComponent } from './post-view/post-view.component';


const routes: Routes = [
  {
    path: 'post-list',
    component: PostListComponent
  },
{
    path: 'post-create',
    component: PostCreateComponent
},
{
    path:'post-update',
    component: PostUpdateComponent
},
{
    path:'post-view',
    component: PostViewComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostRoutingModule { }
