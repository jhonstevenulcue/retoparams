import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentsCreateComponent } from './comments-create/comments-create.component';
import { CommentsListComponent } from './comments-list/comments-list.component';
import { CommentsUpdateComponent } from './comments-update/comments-update.component';
import { CommentsViewComponent } from './comments-view/comments-view.component';
import { CommentsRoutingModule } from './comments-routing.module'
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: 
  [CommentsCreateComponent, 
  CommentsListComponent, 
  CommentsUpdateComponent, 
  CommentsViewComponent],
  imports: [
    CommonModule,
    CommentsRoutingModule,
    ReactiveFormsModule
  ]
})
export class CommentsModule { }
