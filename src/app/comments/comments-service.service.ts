import { Injectable, Component } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Comments } from './interfaz/Comments';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class CommentsServiceService {

  constructor( private http: HttpClient ) { }

  public query(): Observable<Comments[]> {
    return this.http.get<Comments[]>(`${environment.ENN_POINT}/comments`)
    .pipe(map(res => {
      return res;
    }));
  }

  public getCommentsByPostId(postId: string): Observable<Comments>{
    let params = new HttpParams();
    params = params.append('postId',postId);
    console.warn('PARAMS ',params);
    return this.http.get<Comments>(`${environment.ENN_POINT}/comments`,{params: params})
      .pipe(map(res => {
        return res;
      }));
    }
}
