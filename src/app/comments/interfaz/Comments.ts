export interface Comments{
    postId: string;
    id: number;
    name: string;
    email: string;
    body: string;

}