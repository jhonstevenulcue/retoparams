import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommentsListComponent } from './comments-list/comments-list.component';
import { CommentsCreateComponent } from './comments-create/comments-create.component';
import { CommentsUpdateComponent } from './comments-update/comments-update.component';
import { CommentsViewComponent } from './comments-view/comments-view.component';


const routes: Routes = [
  {
    path: 'comments-list',
    component: CommentsListComponent
  },
{
    path: 'comments-create',
    component: CommentsCreateComponent
},
{
    path:'comments-update',
    component: CommentsUpdateComponent
},
{
    path:'comments-view',
    component: CommentsViewComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommentsRoutingModule { }
