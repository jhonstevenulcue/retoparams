import { Component, OnInit } from '@angular/core';
import { Comments } from '../interfaz/Comments';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CommentsServiceService} from '../comments-service.service'

@Component({
  selector: 'app-comments-create',
  templateUrl: './comments-create.component.html',
  styleUrls: ['./comments-create.component.styl']
})
export class CommentsCreateComponent implements OnInit {

   commentsCreateFormGroup: FormGroup;

  comments : Comments;

  statusSearchPostId: Boolean = false;

  formsearchComments = this.formBuilder.group({
    postId: ['']
  });

  constructor(private formBuilder: FormBuilder, private CommentsServiceService: CommentsServiceService) { }

  ngOnInit() {
  }

  searchPostId(){
    console.warn('DATA', this.formsearchComments.value);
    this.CommentsServiceService.getCommentsByPostId(this.formsearchComments.value.postId)
    .subscribe(res =>{
      console.warn('reponse comments by postId ', res);
      this.comments = res;
      this.statusSearchPostId = true;
    }, error => {
      console.warn('No encontrado ', error);
      this.comments = null;
      this.statusSearchPostId = false;
    });
  }

}
