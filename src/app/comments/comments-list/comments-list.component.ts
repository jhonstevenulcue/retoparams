import { Component, OnInit } from '@angular/core';
import { Comments } from '../interfaz/Comments';
import { CommentsServiceService } from '../comments-service.service';

@Component({
  selector: 'app-comments-list',
  templateUrl: './comments-list.component.html',
  styleUrls: ['./comments-list.component.styl']
})
export class CommentsListComponent implements OnInit {

  public commentsList: Comments[];

  constructor(private commentsServe: CommentsServiceService) { }

  ngOnInit() {
    this.commentsServe.query()
    .subscribe(res => {
      this.commentsList = res;
      console.log('response data', res);
    },
    error => console.error('error', error)
    );
  }

}
