import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'comments',
    loadChildren: () => import('./comments/comments.module')
    .then(modulo => modulo.CommentsModule)
  },
  {
    path: 'post',
    loadChildren: () => import('./post/post.module')
    .then(modulo => modulo.PostModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
